from Crypto.Cipher import AES
from Crypto import Random
import datetime
import pickle
from customAES import encryptData, decryptData

class Entry():
   def __init__(self, Company, Password, Category, Notes, CreateDate):
      self.Company = Company
      self.Password = Password
      self.Category = Category
      self.Notes = Notes
      self.CreateDate = CreateDate

class Database():
   def __init__(self, masterPassword):
      self.__masterPassword = self.addPadding(masterPassword)
      self.__encryption_suite = AES.new(self.__masterPassword, AES.MODE_CBC, self.getIV())
      self.__entryList = self.decryptLoad()

   def addEntry(self, Company, Password, Category, Notes, CreateDate=str(datetime.date.today())):
      encryptedPassword = self.__encryption_suite.encrypt(self.addPadding(Password))
      self.__entryList.append(Entry(Company, encryptedPassword, Category, Notes, CreateDate))

   def getEntryByCompany(self, Company):
      results = []
      for entry in self.__entryList:
         if entry.Company == Company:
            results.append(entry)
      if(len(results)==0):
         print "No results for " + Company
      else:
         for index in range(len(results)):
            print "["+str(index)+"] " + results[index].Company + " " + results[index].Category + " " + results[index].CreateDate
         des = raw_input("Please type index of desired result: ")
         print self.__encryption_suite.decrypt(results[int(des)].Password)
      results = [] #for garbage
      #garbagecollect


   def addPadding(self, inputString):
      length = len(inputString) % 16
      if(length == 0):
         return inputString
      charsToAdd = 16 - (len(inputString) % 16)
      charLibrary = "               "
      return inputString + charLibrary[0:charsToAdd]

   def encryptSave(self):
      file = open('data/data.pass', 'w+')
      data = self.addPadding(pickle.dumps(self.__entryList))

      print "*************************"
      print data
      print "*************************"
      print self.__encryption_suite.decrypt(self.__encryption_suite.encrypt(data))
      #file.write(self.__encryption_suite.encrypt(data))
      #file.write(pickle.dumps(self.__entryList))
      file.close()
      #garbagecollect

   def decryptLoad(self):
      try:
         file = open('data/data.pass', 'r')
      except:
         print("data file not found")

      try:
         print self.__encryption_suite.decrypt(file.read())
         #return pickle.loads(self.__encryption_suite.decrypt(file.read()))
         return pickle.loads(file.read())
      except EOFError:
         return []

   def getIV(self):
      iv = None
      try:
         file = open("data/iv.pass", "r")
         iv = file.read()
      except:
         file = open("data/iv.pass", "w+")
         iv = Random.new().read(AES.block_size)
         file.write(iv)
      file.close()
      return iv 